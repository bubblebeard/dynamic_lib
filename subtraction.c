#include "complex.h"

complex Subtraction(complex var1, complex var2) {
    complex result;
    result.real = var1.real - var2.real;
    result.img = var1.img - var2.img;
    return result;
}