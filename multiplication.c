#include "complex.h"

complex Multiplication(complex var1, complex var2) {
    complex result;
    result.real = var1.real * var2.real - var1.img * var2.img;
    result.img = var1.real * var2.img - var2.real * var1.img;
    return result;
}
