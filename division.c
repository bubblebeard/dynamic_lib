#include "complex.h"

complex Division(complex var1, complex var2) {
    complex result;
    result.real = (var1.real * var2.real + var1.img * var2.img) / (var2.real * var2.real + var2.img * var2.img);
    result.img = (var2.real * var1.img - var1.real * var2.img) / (var2.real * var2.real + var2.img * var2.img);
    return result;
}
