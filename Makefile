PLUGIN = /plugin

main: main.c
	gcc main.c -o main -ldl
libdiv.so: division.o
	gcc -std=c99 -shared division.o -o .$(PLUGIN)/libdiv.so
libmult.so: multiplication.o
	gcc -std=c99 -shared multiplication.o -o .$(PLUGIN)/libmult.so
libsub.so: subtraction.o
	gcc -std=c99 -shared subtraction.o -o .$(PLUGIN)/libsub.so
libadd.so: addition.o
	gcc -std=c99 -shared addition.o -o .$(PLUGIN)/libadd.so
addition.o subtraction.o multiplication.o division.o: addition.c subtraction.c multiplication.c division.c
	gcc -std=c99 -fPIC -c addition.c subtraction.c multiplication.c division.c
clean:
	rm -f main addition.o subtraction.o multiplication.o division.o
