#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>
#include <dlfcn.h>
#include <unistd.h>
#include "complex.h"

#define PATH_MAX 4096
  
int main(int argc, char** argv) {
    char choice;
    char buf[PATH_MAX + 1];
    char path[PATH_MAX +1];
    void *add_handler, *sub_handler, *mult_handler, *div_handler;
    complex (*function)(complex var1, complex var2);
    complex var1, var2, result;
    pid_t pid = getpid();
    
    sprintf(path, "/proc/%d/exe", pid);
    readlink(path, buf, PATH_MAX);
    char *pos = strrchr(buf, '/');
    if(pos != NULL) {
        *pos = '\0';
    }
    strcpy(path, buf);
    add_handler = dlopen(strcat(path, "/plugin/libadd.so"), RTLD_LAZY);
    printf("%s\n",path);
    if (!add_handler) {
        fprintf(stderr,"libadd dlopen() error: %s\n", dlerror());
    }
    strcpy(path, buf);
    sub_handler = dlopen(strcat(path, "/plugin/libsub.so"), RTLD_LAZY);
    if (!sub_handler) {
        fprintf(stderr,"libsub dlopen() error: %s\n", dlerror());
    }
    strcpy(path, buf);
    mult_handler = dlopen(strcat(path, "/plugin/libmult.so"), RTLD_LAZY);
    if (!mult_handler) {
        fprintf(stderr,"libmult dlopen() error: %s\n", dlerror());
    }
    strcpy(path, buf);
    div_handler = dlopen(strcat(path, "/plugin/libdiv.so"), RTLD_LAZY);
    if (!div_handler) {
        fprintf(stderr,"libdiv dlopen() error: %s\n", dlerror());
    }
    while (1) {
        printf("select command:\n");
        if (add_handler)
            printf("a - addition\n");
        if (sub_handler)
            printf("s - subtraction\n");
        if (mult_handler)
            printf("m - multiplication\n");
        if (div_handler)
            printf("d - division\n");
        printf("q - quit\n");
        scanf(" %c", &choice);
        switch (choice) {
            case 'A':
            case 'a':
                printf("enter first operand\n");
                printf("real part - ");
                scanf("%f", &var1.real);
                printf("imaginary part - ");
                scanf("%f", &var1.img);
                printf("enter second operand\n");
                printf("real part - ");
                scanf("%f", &var2.real);
                printf("imaginary part - ");
                scanf("%f", &var2.img);
                function = dlsym(add_handler, "Addition");
                result = (*function)(var1, var2);
                printf("result - %f + i%f\n", result.real, result.img);
                break;
            case 'S': 
            case 's':
                printf("enter first operand\n");
                printf("real part - ");
                scanf("%f", &var1.real);
                printf("imaginary part - ");
                scanf("%f", &var1.img);
                printf("enter second operand\n");
                printf("real part - ");
                scanf("%f", &var2.real);
                printf("imaginary part - ");
                scanf("%f", &var2.img);
                function = dlsym(sub_handler, "Subtraction");
                result = (*function)(var1, var2);
                printf("result - %f + i%f\n", result.real, result.img);
                break;
            case 'M':
            case 'm':
                printf("enter first operand\n");
                printf("real part - ");
                scanf("%f", &var1.real);
                printf("imaginary part - ");
                scanf("%f", &var1.img);
                printf("enter second operand\n");
                printf("real part - ");
                scanf("%f", &var2.real);
                printf("imaginary part - ");
                scanf("%f", &var2.img);
                function = dlsym(mult_handler, "Multiplication");
                result = (*function)(var1, var2);
                printf("result - %f + i%f\n", result.real, result.img);
                break;
            case 'D':
            case 'd':
                printf("enter first operand\n");
                printf("real part - ");
                scanf("%f", &(var1.real));
                printf("imaginary part - ");
                scanf("%f", &(var1.img));
                printf("enter second operand\n");
                printf("real part - ");
                scanf("%f", &(var2.real));
                printf("imaginary part - ");
                scanf("%f", &(var2.img));
                function = dlsym(div_handler, "Division");
                result = (*function)(var1, var2);
                printf("result - %f + i%f\n", result.real, result.img);
                break;
            case 'Q':
            case 'q':
                if (add_handler)
                    dlclose(add_handler);
                if (sub_handler)
                    dlclose(sub_handler);
                if (mult_handler)
                    dlclose(mult_handler);
                if (div_handler)
                    dlclose(div_handler);
                exit(1);
            default:
                printf("unknown command\n");
                break;
        }
    }

    return (EXIT_SUCCESS);
}